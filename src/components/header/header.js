import React from "react";
import "./header.scss";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

function Header() {
  const products = useSelector((state) => state.products);

  return (
    <header className="header">
      <nav>
        <ul className="nav-menu">
          <li className="nav-element">
            <Link className="nav-link" to="/">
              Home
            </Link>
          </li>
          <li className="nav-element">
            <Link className="nav-link" to="/cart">
              Cart
            </Link>
          </li>
          <li className="nav-element">
            <Link className="nav-link" to="/favorite">
              Favorite
            </Link>
          </li>
        </ul>
      </nav>
      <div className="icons-container">
        <img className="star-icon" src="/images/star.svg" alt="Star Icon" />
        <div className="star-count">
          {products.filter((product) => product.isFavorite).length}
        </div>
        <img className="cart-icon" src="/images/cart.svg" alt="Cart Icon" />
        <div className="cart-count">
          {products.filter((product) => product.isAddedToCart).length}
        </div>
      </div>
    </header>
  );
}

export default Header;
