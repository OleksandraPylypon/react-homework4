import React, { useEffect } from "react";
import Modal from "../modal/Modal";
import { useSelector, useDispatch } from "react-redux";
import ProductsCard from "./productsCard";
import "./Products.scss";
import { useLocation } from "react-router-dom";
import { addProducts } from "../../rootReducers";
import useLocalStorage from "../../hooks/useLocalStorage";

function ProductsList() {
  const products = useSelector((state) => state.products);
  const dispatch = useDispatch();
  const location = useLocation();

  const [favoriteProducts] = useLocalStorage("favorite-products", []);
  const [cartProducts] = useLocalStorage("cart-products", []);

  const productsToShow = products.filter((product) => {
    if (location.pathname === "/cart") {
      return product.isAddedToCart;
    }

    if (location.pathname === "/favorite") {
      return product.isFavorite;
    }

    return true;
  });

  useEffect(() => {
    fetch("./products.json")
      .then((response) => response.json())
      .then((data) => {
        dispatch(
          addProducts(
            data.map((product) => ({
              ...product,
              isFavorite: favoriteProducts?.includes(product.name),
              isAddedToCart: cartProducts?.includes(product.name),
              isModalOpen: false,
              isDialogOpen: false,
            }))
          )
        );
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  if (!productsToShow?.length) {
    return <div className="not-found">Products not found!</div>;
  }

  return (
    <div className="products-list">
      {productsToShow.map((product, index) => (
        <ProductsCard
          key={index}
          imagePath={product.imagePath}
          name={product.name}
          articleNumber={product.articleNumber}
          color={product.color}
          price={product.price}
          isFavorite={product.isFavorite}
          isAddedToCart={product.isAddedToCart}
          isModalOpen={product.isModalOpen}
          isDialogOpen={product.isDialogOpen}
        />
      ))}
      <Modal />
    </div>
  );
}

export default ProductsList;
