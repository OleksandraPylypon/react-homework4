import React, { useEffect } from "react";
import styles from "./Modal.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { changeModal } from "../../rootReducers";

const Modal = () => {
  const modal = useSelector((state) => state.modal);
  const dispatch = useDispatch();

  useEffect(() => {
    if (modal.isOpen) {
      setTimeout(() => {
        dispatch(changeModal({ text: "", isOpen: false }));
      }, 1000);
    }
  }, [modal.isOpen]);

  if (!modal.isOpen) {
    return null;
  }

  return (
    <div className={styles["modal-overlay"]}>
      <div className={styles.modal}>
        <div className={styles["modal-content"]}>{modal.text}</div>
      </div>
    </div>
  );
};

export default Modal;
