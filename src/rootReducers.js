import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  products: [],
  modal: { text: "", isOpen: false },
};

const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    addProducts: (state, action) => {
      state.products = action.payload;
    },
    toggleFavorites: (state, action) => {
      state.products = state.products.map((product) => {
        if (product.name === action.payload) {
          product.isFavorite = !product.isFavorite;
        }

        return product;
      });

      localStorage.setItem(
        "favorite-products",
        JSON.stringify(
          state.products
            .filter((product) => product.isFavorite)
            .map((product) => product.name)
        )
      );
    },
    toggleCart: (state, action) => {
      state.products = state.products.map((product) => {
        if (product.name === action.payload) {
          if (!product.isAddedToCart) {
            state.modal = {
              text: `You have added ${action.payload} to your cart.`,
              isOpen: true,
            };
          }
          product.isAddedToCart = !product.isAddedToCart;
        }

        return product;
      });

      localStorage.setItem(
        "cart-products",
        JSON.stringify(
          state.products
            .filter((product) => product.isAddedToCart)
            .map((product) => product.name)
        )
      );
    },
    changeModal: (state, action) => {
      state.modal = action.payload;
    },
  },
});

export const { addProducts, toggleFavorites, toggleCart, changeModal } =
  productSlice.actions;
export default productSlice.reducer;
